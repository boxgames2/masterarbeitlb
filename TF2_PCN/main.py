import os

from matplotlib import pyplot as plt
import PCN_cd
import tensorflow as tf
import numpy as np
import modelloader
from io_util import read_pcd, save_pcd

from TF_Layer_utils.DECODER import DECODER
from open3d_util import plot_pcd, show_pcd


def create_plots(partial, complete):
    fig = plt.figure(figsize=(8, 4))
    ax = fig.add_subplot(121, projection='3d')
    plot_pcd(ax, partial)
    ax.set_title('Input')
    ax = fig.add_subplot(122, projection='3d')
    plot_pcd(ax, complete)
    ax.set_title('Output')
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0)
    

partial = read_pcd("./demo_data/airplane.pcd")
pointcloud1 = partial
#pointcloud1 = np.array([[0,0,0],[1,1,1],[2,2,2]])
data = pointcloud1
data = np.expand_dims(data, axis=0)
data = data.astype(np.float32)
pointcloud_shapes = [pointcloud1.shape[0]]


#create the model structure
model = PCN_cd.create_pcn_cd_model(pointcloud_shapes)

#print(model.summary())

# for layer in model.layers: 
#     layerconf = layer.get_config()
#     layer_w = layer.get_weights()
#     print("layer {} \n Weights: {}".format(layerconf, len(layer_w)))


model = modelloader.load_weights_and_biases(model) 

#save the model
#model.save("./pcn",overwrite=True, include_optimizer=False)

#testing 

complete=model.predict(data)
#potputs prüfen
complete = np.squeeze(complete)
print(complete.shape)
create_plots(partial, complete)

show_pcd(partial)
show_pcd(complete)
plt.show()

output_file = os.path.join("./demo_data/", "completed_airplane" + '.pcd')
save_pcd(output_file, complete)

output_file = os.path.join("./demo_data/", "completed_airplane" + '.png')
plt.savefig(output_file)
        