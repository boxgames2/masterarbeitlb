from MLP1DCONV import MLP1DCONV
from MLP import MLP
from DECODER import DECODER
from PointPooling import DEPTHMAXPOOLING
import tensorflow as tf
from tensorflow.keras.layers import MaxPooling1D
import numpy as np 
from PointPooling import DEPTHUNPOOLING
'''
Ich versuche im Testscript zunächst den Encoder des PCN_cd(z.47-55) nachzubauen.
Vorlage hierfür ist im pcn repo: ./models/pcn_cd.py.
Dieser besteht aus einer MLP_conv Struktur (pcn repo ./tf_utils.py),
gefolgt con einem Pointmaxpooling innerhalb eines point_unpooling.
Anschließend eine weitere MLP_conv struktur und ein Point_Maxpooling.
'''
#Variables used by the decoder
resolution_coarse = 1024
grid_size = 4
#the fine resolution is the squared Gridsize times the corse Resolution.
resolution_fine = grid_size ** 2 * resolution_coarse

pointcloud1 = np.array([[0,0,0],[1,1,1]])
data = pointcloud1
data = np.expand_dims(data, axis=0)
data = data.astype(np.float32)
pointcloud_shapes = [pointcloud1.shape[0]]

#Creation of the mlp_conv layers for the encoder.
encoder_mlp_conv1 = MLP1DCONV(layer_dims=[128,256])
encoder_mlp_conv2 = MLP1DCONV(layer_dims=[512, 1024])

#Creation of the mlp_conv layers for the encoder.
decoder_mlp1 = MLP(layer_dims=[1024,1024], activations=[tf.nn.relu, tf.nn.relu])
decoder_mlp_conv1 = MLP1DCONV(layer_dims=[512,512,3])

#Geplant ist es das Model tf2 gerecht zu einem keras.Sequential umzubauen
model = tf.keras.Sequential()

#Encoder
model.add(encoder_mlp_conv1)
#poitwise maxpooling and unpooling
model.add(DEPTHMAXPOOLING(n_points=pointcloud_shapes))
model.add(DEPTHUNPOOLING(n_points=pointcloud_shapes))
#Zweite Hälfte des Encooders
model.add(encoder_mlp_conv2)
model.add(DEPTHMAXPOOLING(n_points=pointcloud_shapes))

#Decoder
#model.add(MLP(layer_dims=[1024,1024,resolution_coarse*3], activations=[tf.nn.relu, tf.nn.relu]))
model.add(DECODER(n_coarse=resolution_coarse,n_fine=resolution_fine, grid_size=grid_size))

#Da noch kein Inputlayer Existier, muss ein Inputlayer mit der gewünschten Shape erstellt werden.
model.build([1, 2, 3])

print(model.summary())



model.compile(optimizer="Adam", loss="mse", metrics=["mae"])
pred = model.predict(data)
print(pred.shape)
print(pred)