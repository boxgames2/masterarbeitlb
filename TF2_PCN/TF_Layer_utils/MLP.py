import tensorflow as tf


class MLP(tf.keras.layers.Layer):
    def __init__(self, layer_dims, activations, name=None):
        self.layer_dims=layer_dims
        self.activation= activations
        super(MLP, self).__init__()
        if name is None:
            name = ""
        else:
            name += "_"
        self.layers = []
        if len(layer_dims) < len(activations):
                activations = activations[:len(layer_dims)]
        elif len(layer_dims) > len(activations):
            layers2add = len(layer_dims) - len(activations)
            print("Warning: Will add {0} linear activation functions in layer {1}".format(layers2add, name))
            activations.extend(layers2add*[None])
        for layer_dim, act, i in zip(layer_dims[:-1], activations[:-1], range(len(layer_dims)-1)):
            d = tf.keras.layers.Dense(
                units=layer_dim,
                activation=act,
                name=name + "dense_" + str(i))
            self.layers.append(d)
        d = tf.keras.layers.Dense(
            units=layer_dims[-1],
            activation=activations[-1],
            name=name + "dense_" + str(len(layer_dims)-1))
        self.layers.append(d)

    def get_config(self):
                config = super().get_config()
                config.update({
                    "layer_dims": self.layer_dims,
                    "activations": self.activations,
                    "name": self.name,
                })
                return config
    def build(self, input_shape):
        pass

    def set_layer_weights(self,layer_index, weights, biases):
            """Sets the biases and weights of a specific intern layer.

            Args:
                layer_index ([type]): [description]
                weights ([type]): [description]
                biases ([type]): [description]
            """
            layer = self.layers[layer_index]
            layer.set_weights([weights,biases])
            
    def print_all_layer_weights(self):
             for layer in self.layers:
                 print("decoder mlp layer weights:",layer.weights)
    def call(self, inputs):
        # print("mlp inputshape = ",inputs.shape)
        # print("used dims: ",self.layer_dims)
        # print("used activation: ",self.activation)
        for layer in self.layers:
            inputs = layer(inputs)
        # print("mlp outputshape = ",inputs.shape)
        return inputs