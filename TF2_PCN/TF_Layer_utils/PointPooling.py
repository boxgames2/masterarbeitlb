import tensorflow as tf
max_pool_inputs =[]

class DEPTHMAXPOOLING(tf.keras.layers.Layer):
    '''
    A layer that performs max pooling featurewise over all layers.
    '''
    def __init__(self, n_points):
        super().__init__()
        self.n_points = n_points
        
    def get_config(self):
            config = super().get_config()
            config.update({
                "n_points": self.n_points,
            })
            return config
        
    def call(self, inputs):   
        global max_pool_inputs
        max_pool_inputs = inputs
       
        outputs = [tf.reduce_max(f, axis=1, keepdims=True) for f in tf.split(inputs, self.n_points, axis=1)]
        outputs = tf.concat(outputs, axis=0)
        #print("maxpoolshape: ",outputs.shape)
        return outputs
    
    
    
class DEPTHUNPOOLING(tf.keras.layers.Layer):
    '''
    A layer that splits combined pointclouds
    '''
    def __init__(self, n_points):
        super().__init__()
        self.n_points = n_points
        
    def get_config(self):
            config = super().get_config()
            config.update({
                "n_points": self.n_points,
            })
            return config
        
    def call(self, inputs):
        inputs = tf.split(inputs, inputs.shape[0], axis=0)
        outputs = [tf.tile(f, [1, self.n_points[i], 1]) for i,f in enumerate(inputs)]
        outputs = tf.concat(outputs, axis=1)
        return outputs
    
class CONCATINPUTS(tf.keras.layers.Layer):
    '''
    A layer that splits combined pointclouds
    '''
    def __init__(self):
        super().__init__()
        
    def get_config(self):
            config = super().get_config()
            config.update({
                "to_concat_inputs": self.to_concat_inputs,
            })
            return config
        
    def call(self, inputs):
        global max_pool_inputs
        self.to_concat_inputs = max_pool_inputs
        outputs = tf.concat([inputs ,self.to_concat_inputs], axis=2)
        return outputs