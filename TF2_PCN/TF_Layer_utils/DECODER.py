import tensorflow as tf
from .MLP1DCONV import MLP1DCONV
from .MLP import MLP

class DECODER(tf.keras.layers.Layer):
    '''
    A layer that performs max pooling featurewise over all layers.
    '''

    def __init__(self, n_coarse, grid_size, n_fine):
        super().__init__()
        self.n_coarse = n_coarse
        self.grid_size = grid_size
        self.n_fine = n_fine
        self.decoder_mlp1 = MLP(layer_dims=[1024,1024,n_coarse * 3], activations=[tf.nn.relu, tf.nn.relu, tf.nn.relu])
        self.decoder_mlp_conv1 = MLP1DCONV(layer_dims=[512,512,3])
        
    def set_mlp_conv_layer_weights_and_biases(self,layer_index, weights, biases):
        """Sets the biases and weights of a specific intern layer of the mlp1Dconv part.

        Args:
            layer_index ([type]): [description]
            weights ([type]): [description]
            biases ([type]): [description]
        """
        self.decoder_mlp_conv1.set_layer_weights(layer_index, weights, biases)
    
    def set_mlp_layer_weights_and_biases(self,layer_index, weights, biases):
        """Sets the biases and weights of a specific intern layer of the mlp part.

        Args:
            layer_index ([type]): [description]
            weights ([type]): [description]
            biases ([type]): [description]
        """
        self.decoder_mlp1.set_layer_weights(layer_index, weights, biases)
   
    def get_config(self):
            config = super().get_config()
            config.update({
                "n_coarse": self.n_coarse,
                "grid_size": self.grid_size,
                "n_fine": self.n_fine,
            })
            return config
        
    def call(self, inputs):  
        
        #print("shape f ",inputs.shape)
        #reshape inputs [1,1,x] to [1,x]
        inputs = tf.squeeze(inputs)
        inputs = tf.expand_dims(inputs, axis=0)
        #print("new shape f ",inputs.shape)
        coarse = self.decoder_mlp1(inputs)
        #print("shape coarse after raw mlp",coarse.shape)
        #print("n_coarse ", self.n_coarse)
        #self.decoder_mlp1.print_all_layer_weights()
        # folding
        coarse = tf.reshape(coarse, [-1, self.n_coarse, 3])
        #print("shape coarse ",coarse.shape)
        grid = tf.meshgrid(tf.linspace(-0.05, 0.05, self.grid_size), tf.linspace(-0.05, 0.05, self.grid_size))
        grid = tf.expand_dims(tf.reshape(tf.stack(grid, axis=2), [-1, 2]), 0)
        grid_feat = tf.tile(grid, [inputs.shape[0], self.n_coarse, 1])

        point_feat = tf.tile(tf.expand_dims(coarse, 2), [1, 1, self.grid_size ** 2, 1])
        point_feat = tf.reshape(point_feat, [-1, self.n_fine, 3])

        global_feat = tf.tile(tf.expand_dims(inputs, 1), [1, self.n_fine, 1])

        feat = tf.concat([grid_feat, point_feat, global_feat], axis=2)

        center = tf.tile(tf.expand_dims(coarse, 2), [1, 1, self.grid_size ** 2, 1])
        center = tf.reshape(center, [-1, self.n_fine, 3])
        fine = self.decoder_mlp_conv1(feat) + center
        return fine