
from copy import deepcopy
import tensorflow as tf
from .MLP1DCONV import MLP1DCONV
from .MLP import MLP
from TF_Layer_utils.PointPooling import DEPTHMAXPOOLING
from TF_Layer_utils.PointPooling import DEPTHUNPOOLING

class ENCODER(tf.keras.layers.Layer):
    '''
    A combination of layers to create the Encoder.
    '''
    

    def __init__(self, mlp_conv0_dims=[128,256], mlp_conv1_dims=[512, 1024],n_points=0):
        super().__init__()
        self.mlp_conv0_dims = mlp_conv0_dims
        self.mlp_conv1_dims = mlp_conv1_dims      
        self.n_points = n_points   
        self.non_encoded_inputs = []
        #Creation of the mlp_conv layers for the encoder.
        encoder_mlp_conv1 = MLP1DCONV(layer_dims=self.mlp_conv0_dims)
        encoder_mlp_conv2 = MLP1DCONV(layer_dims=self.mlp_conv1_dims)
        self.layers_first_part = []
        self.layers_secound_part = []
        self.layers_first_part.append(encoder_mlp_conv1)
        self.layers_first_part.append(DEPTHMAXPOOLING(n_points=n_points))
        self.layers_first_part.append(DEPTHUNPOOLING(n_points=n_points))
        self.layers_secound_part.append(encoder_mlp_conv2)
        self.layers_secound_part.append(DEPTHMAXPOOLING(n_points=n_points))
        
    def get_config(self):
            config = super().get_config()
            config.update({
                "mlp_conv0_dims": self.mlp_conv0_dims,
                "mlp_conv1_dims": self.mlp_conv1_dims,
            })
            return config
        
    def concat_layer(self, inputs):
        return tf.concat([inputs, self.non_encoded_inputs], axis=2)
    
    def build(self, input_shape):
        pass
    
    def call(self, inputs):  
        self.non_encoded_inputs = []
        
        for layer in self.layers_first_part:
            outputs = layer(inputs)
            if self.non_encoded_inputs == []:
                self.non_encoded_inputs = layer(inputs)
        print(outputs.shape())
        outputs = self.concat_layer(outputs)
        for layer in self.layers_secound_part:
            outputs = layer(outputs)
        return outputs
    
    