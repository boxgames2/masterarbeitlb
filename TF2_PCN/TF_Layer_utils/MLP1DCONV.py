import tensorflow as tf


class MLP1DCONV(tf.keras.layers.Layer):
    def __init__(self, layer_dims, name=None, is_first = False):
        super(MLP1DCONV, self).__init__()
        counter = 0
        self.layer_dims = layer_dims
        self.layers = []
        for layer_dim in layer_dims[:-1]:
            if counter == 0 and is_first:
                c1d = tf.keras.layers.Conv1D(filters=layer_dim, kernel_size=1, activation=tf.nn.relu)
            else:
                c1d = tf.keras.layers.Conv1D(filters=layer_dim, kernel_size=1, activation=tf.nn.relu)
            self.layers.append(c1d)
            
            counter +=1
        c1d = tf.keras.layers.Conv1D(filters=layer_dims[-1], kernel_size=1, activation=None)
        self.layers.append(c1d)

    def build(self, input_shape):
        pass

    def get_config(self):
            config = super().get_config()
            config.update({
                "name": self.name,
                "layer_dims": self.layer_dims,
            })
            return config
        
    def get_weights(self):
        weights=[]
        for layer in self.layers:
            weights.append(layer.get_weights())
            #print("Weight:",layer.get_weights()[0])
            #print("bias:",layer.get_weights()[1])
        return weights
    
    def set_layer_weights(self,layer_index, weights, biases):
        """Sets the biases and weights of a specific intern layer.

        Args:
            layer_index ([type]): [description]
            weights ([type]): [description]
            biases ([type]): [description]
        """
        layer = self.layers[layer_index]
        layer.set_weights([weights,biases])
        #print("Weight:",layer.get_weights()[0])
        #print("bias:",layer.get_weights()[1])
    
    def call(self, inputs):
        for layer in self.layers:
            inputs = layer(inputs)
        return inputs