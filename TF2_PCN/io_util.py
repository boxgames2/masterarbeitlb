from fnmatch import translate
import numpy as np
import open3d as o3d


def read_pcd(filename):
    total_points = 0
    pcd = o3d.io.read_point_cloud(filename)
    
    #create boundingbox and adjust input
    bbox = o3d.geometry.OrientedBoundingBox.create_from_points(pcd.points)
    
    # total_points += np.array(pcd.points).shape[0]

    # Calculate center, rotation and scale
    center = bbox.get_center()
    pcd.translate(-center)
    
    #calculate the scale with 2 opposite corners on one plane
    #using a new bounding box after the translation
    bbox = o3d.geometry.OrientedBoundingBox.create_from_points(pcd.points)
    scale = bbox.get_box_points()[0] - bbox.get_box_points()[7] 
    pcd.rotate()
    # yaw = np.arctan2(bbox[3, 1] - bbox[0, 1], bbox[3, 0] - bbox[0, 0])
    # rotation = np.array([[np.cos(yaw), -np.sin(yaw), 0],
    #                     [np.sin(yaw), np.cos(yaw), 0],
    #                     [0, 0, 1]])
    # bbox = np.dot(bbox, rotation)
    # scale = bbox[3, 0] - bbox[0, 0]
    # bbox /= scale

    # pcd = np.dot(pcd - center, rotation) / scale
    # pcd = np.dot(pcd, [[1, 0, 0], [0, 0, 1], [0, 1, 0]])
        
    return np.array(pcd.points)


def save_pcd(filename, points):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    o3d.io.write_point_cloud(filename, pcd)