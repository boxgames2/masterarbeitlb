
from TF_Layer_utils.MLP1DCONV import MLP1DCONV
from TF_Layer_utils.MLP import MLP
from TF_Layer_utils.DECODER import DECODER
from TF_Layer_utils.PointPooling import DEPTHMAXPOOLING
from TF_Layer_utils.PointPooling import DEPTHUNPOOLING
from TF_Layer_utils.PointPooling import CONCATINPUTS
import tensorflow as tf
import numpy as np 


def create_pcn_cd_model(pointcloud_shapes):
    #Variables used by the decoder
    resolution_coarse = 1024
    grid_size = 4
    #the fine resolution is the squared Gridsize times the corse Resolution.
    resolution_fine = grid_size ** 2 * resolution_coarse

    #Creation of the mlp_conv layers for the encoder.
    mlp_conv0_dims=[128,256]
    mlp_conv1_dims=[512, 1024]
    encoder_mlp_conv1 = MLP1DCONV(layer_dims=mlp_conv0_dims)
    encoder_mlp_conv2 = MLP1DCONV(layer_dims=mlp_conv1_dims)
    #Creation of the mlp_conv layers for the encoder.
    decoder_mlp1 = MLP(layer_dims=[1024,1024], activations=[tf.nn.relu, tf.nn.relu])
    decoder_mlp_conv1 = MLP1DCONV(layer_dims=[512,512,3])

    #Geplant ist es das Model tf2 gerecht zu einem keras.Sequential umzubauen
    model = tf.keras.Sequential()

    #Encoder
    #model.add(ENCODER(mlp_conv0_dims,mlp_conv1_dims,pointcloud_shapes))
    model.add(encoder_mlp_conv1)
    #poitwise maxpooling and unpooling
    model.add(DEPTHMAXPOOLING(n_points=pointcloud_shapes))
    model.add(DEPTHUNPOOLING(n_points=pointcloud_shapes))
    model.add(CONCATINPUTS())
    #Zweite Hälfte des Encooders
    model.add(encoder_mlp_conv2)
    model.add(DEPTHMAXPOOLING(n_points=pointcloud_shapes))

    #Decoder
    #model.add(MLP(layer_dims=[1024,1024,resolution_coarse*3], activations=[tf.nn.relu, tf.nn.relu]))
    model.add(DECODER(n_coarse=resolution_coarse,n_fine=resolution_fine, grid_size=grid_size))

    #Da noch kein Inputlayer Existier, muss ein Inputlayer mit der gewünschten Shape erstellt werden.
    model.build([1, None, 3])

    model.compile(optimizer="Adam", loss="mse", metrics=["mae"])
    return model