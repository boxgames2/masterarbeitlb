
from statistics import mode
import pickle
from turtle import shape
import numpy as np

def load_weights_and_biases(model):
    model_weight_and_bias_data = open("model_weight_and_bias_data.pkl", "rb")
    model_weight_and_bias_dict = pickle.load(model_weight_and_bias_data)
    weights_to_assign = 0
    encoder_conv_mlp_to_assign = 0
    decoder_mlp_to_assign = 0
    folding_conv_mlp_to_assign = 0
    
    weights_assigned = 0
    encoder_conv_mlp_assigned = 0
    decoder_mlp_assigned = 0
    folding_conv_mlp_assigned = 0
    
    for key,value in model_weight_and_bias_dict.items():
        if key.endswith("weights"):
            weights_to_assign += np.array(value).size
            if key.startswith("encoder"):
                encoder_conv_mlp_to_assign += np.array(value).size
            if key.startswith("decoder"):
                decoder_mlp_to_assign += np.array(value).size
            if key.startswith("folding"):
                folding_conv_mlp_to_assign += np.array(value).size

    #set weights and biases manually once
    #encoder
    print("Encoder 0 layer 0")
    encoder_conv0 = model.get_layer("mlp1dconv")
    weights = (model_weight_and_bias_dict["encoder_0_conv_0_weights"])
    biases = (model_weight_and_bias_dict["encoder_0_conv_0_biases"])
    encoder_conv0.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    print("Encoder 0 layer 1")
    weights = (model_weight_and_bias_dict["encoder_0_conv_1_weights"])
    biases =  (model_weight_and_bias_dict["encoder_0_conv_1_biases"])
    encoder_conv0.set_layer_weights(1, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    print("Encoder 1 layer 0")
    encoder_conv1 = model.get_layer("mlp1dconv_1")
    weights = (model_weight_and_bias_dict["encoder_1_conv_0_weights"])
    biases = (model_weight_and_bias_dict["encoder_1_conv_0_biases"])
    encoder_conv1.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    print("Encoder 1 layer 1")
    weights = (model_weight_and_bias_dict["encoder_1_conv_1_weights"])
    biases =  (model_weight_and_bias_dict["encoder_1_conv_1_biases"])
    encoder_conv1.set_layer_weights(1, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size

    #decoder
    decoder = model.get_layer("decoder")
    print("decoder mlp layer 0")
    weights = (model_weight_and_bias_dict["decoder_fc_0_weights"])
    biases =  (model_weight_and_bias_dict["decoder_fc_0_biases"])
    decoder.set_mlp_layer_weights_and_biases(0, weights,biases)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size
    
    print("decoder mlp layer 1")
    weights = (model_weight_and_bias_dict["decoder_fc_1_weights"])
    biases =  (model_weight_and_bias_dict["decoder_fc_1_biases"])
    decoder.set_mlp_layer_weights_and_biases(1, weights,biases)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size
    
    print("decoder mlp layer 2")
    weights = (model_weight_and_bias_dict["decoder_fc_2_weights"])
    biases =  (model_weight_and_bias_dict["decoder_fc_2_biases"])
    decoder.set_mlp_layer_weights_and_biases(2, weights,biases)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size

    print("decoder mlpconv0 layer 0")
    weights = (model_weight_and_bias_dict["folding_conv_0_weights"])
    biases =  (model_weight_and_bias_dict["folding_conv_0_biases"])
    decoder.set_mlp_conv_layer_weights_and_biases(0, weights,biases)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    print("decoder mlpconv0 layer 1")
    weights = (model_weight_and_bias_dict["folding_conv_1_weights"])
    biases =  (model_weight_and_bias_dict["folding_conv_1_biases"])
    decoder.set_mlp_conv_layer_weights_and_biases(1, weights,biases)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    print("decoder mlpconv0 layer 2")
    weights = (model_weight_and_bias_dict["folding_conv_2_weights"])
    biases =  (model_weight_and_bias_dict["folding_conv_2_biases"])
    decoder.set_mlp_conv_layer_weights_and_biases(2, weights,biases)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    print("assigned {} of {} values".format(weights_assigned, weights_to_assign))
    print("encoder assigned {} of {} values".format(encoder_conv_mlp_assigned, encoder_conv_mlp_to_assign))
    print("decoder assigned {} of {} values".format(decoder_mlp_assigned, decoder_mlp_to_assign))
    print("folding assigned {} of {} values".format(folding_conv_mlp_assigned, folding_conv_mlp_to_assign))
    
    return model

    # ModelStructure:
    #     encoder_0/conv_0/biases (DT_FLOAT) [128]
    #     encoder_0/conv_0/weights (DT_FLOAT) [1,3,128]
    #     encoder_0/conv_1/biases (DT_FLOAT) [256]
    #     encoder_0/conv_1/weights (DT_FLOAT) [1,128,256]
        
    #     encoder_1/conv_0/biases (DT_FLOAT) [512]
    #     encoder_1/conv_0/weights (DT_FLOAT) [1,512,512]
    #     encoder_1/conv_1/biases (DT_FLOAT) [1024]
    #     encoder_1/conv_1/weights (DT_FLOAT) [1,512,1024]
        
    #     decoder/fc_0/biases (DT_FLOAT) [1024]
    #     decoder/fc_0/weights (DT_FLOAT) [1024,1024]
    #     decoder/fc_1/biases (DT_FLOAT) [1024]
    #     decoder/fc_1/weights (DT_FLOAT) [1024,1024]
    #     decoder/fc_2/biases (DT_FLOAT) [3072]
    #     decoder/fc_2/weights (DT_FLOAT) [1024,3072]
        
    #     folding/conv_0/biases (DT_FLOAT) [512]
    #     folding/conv_0/weights (DT_FLOAT) [1,1029,512]
    #     folding/conv_1/biases (DT_FLOAT) [512]
    #     folding/conv_1/weights (DT_FLOAT) [1,512,512]
    #     folding/conv_2/biases (DT_FLOAT) [3]
    #     folding/conv_2/weights (DT_FLOAT) [1,512,3]
    
    
    
    
#     full shape
# beta1_power (DT_FLOAT) []
# beta2_power (DT_FLOAT) []
# decoder/fc_0/biases (DT_FLOAT) [1024]
# decoder/fc_0/biases/Adam (DT_FLOAT) [1024]
# decoder/fc_0/biases/Adam_1 (DT_FLOAT) [1024]
# decoder/fc_0/weights (DT_FLOAT) [1024,1024]
# decoder/fc_0/weights/Adam (DT_FLOAT) [1024,1024]
# decoder/fc_0/weights/Adam_1 (DT_FLOAT) [1024,1024]
# decoder/fc_1/biases (DT_FLOAT) [1024]
# decoder/fc_1/biases/Adam (DT_FLOAT) [1024]
# decoder/fc_1/biases/Adam_1 (DT_FLOAT) [1024]
# decoder/fc_1/weights (DT_FLOAT) [1024,1024]
# decoder/fc_1/weights/Adam (DT_FLOAT) [1024,1024]
# decoder/fc_1/weights/Adam_1 (DT_FLOAT) [1024,1024]
# decoder/fc_2/biases (DT_FLOAT) [3072]
# decoder/fc_2/biases/Adam (DT_FLOAT) [3072]
# decoder/fc_2/biases/Adam_1 (DT_FLOAT) [3072]
# decoder/fc_2/weights (DT_FLOAT) [1024,3072]
# decoder/fc_2/weights/Adam (DT_FLOAT) [1024,3072]
# decoder/fc_2/weights/Adam_1 (DT_FLOAT) [1024,3072]

# encoder_0/conv_0/biases (DT_FLOAT) [128]
# encoder_0/conv_0/biases/Adam (DT_FLOAT) [128]
# encoder_0/conv_0/biases/Adam_1 (DT_FLOAT) [128]
# encoder_0/conv_0/weights (DT_FLOAT) [1,3,128]
# encoder_0/conv_0/weights/Adam (DT_FLOAT) [1,3,128]
# encoder_0/conv_0/weights/Adam_1 (DT_FLOAT) [1,3,128]
# encoder_0/conv_1/biases (DT_FLOAT) [256]
# encoder_0/conv_1/biases/Adam (DT_FLOAT) [256]
# encoder_0/conv_1/biases/Adam_1 (DT_FLOAT) [256]
# encoder_0/conv_1/weights (DT_FLOAT) [1,128,256]
# encoder_0/conv_1/weights/Adam (DT_FLOAT) [1,128,256]
# encoder_0/conv_1/weights/Adam_1 (DT_FLOAT) [1,128,256]
# encoder_1/conv_0/biases (DT_FLOAT) [512]
# encoder_1/conv_0/biases/Adam (DT_FLOAT) [512]
# encoder_1/conv_0/biases/Adam_1 (DT_FLOAT) [512]
# encoder_1/conv_0/weights (DT_FLOAT) [1,512,512]
# encoder_1/conv_0/weights/Adam (DT_FLOAT) [1,512,512]
# encoder_1/conv_0/weights/Adam_1 (DT_FLOAT) [1,512,512]
# encoder_1/conv_1/biases (DT_FLOAT) [1024]
# encoder_1/conv_1/biases/Adam (DT_FLOAT) [1024]
# encoder_1/conv_1/biases/Adam_1 (DT_FLOAT) [1024]
# encoder_1/conv_1/weights (DT_FLOAT) [1,512,1024]
# encoder_1/conv_1/weights/Adam (DT_FLOAT) [1,512,1024]
# encoder_1/conv_1/weights/Adam_1 (DT_FLOAT) [1,512,1024]

# folding/conv_0/biases (DT_FLOAT) [512]
# folding/conv_0/biases/Adam (DT_FLOAT) [512]
# folding/conv_0/biases/Adam_1 (DT_FLOAT) [512]
# folding/conv_0/weights (DT_FLOAT) [1,1029,512]
# folding/conv_0/weights/Adam (DT_FLOAT) [1,1029,512]
# folding/conv_0/weights/Adam_1 (DT_FLOAT) [1,1029,512]
# folding/conv_1/biases (DT_FLOAT) [512]
# folding/conv_1/biases/Adam (DT_FLOAT) [512]
# folding/conv_1/biases/Adam_1 (DT_FLOAT) [512]
# folding/conv_1/weights (DT_FLOAT) [1,512,512]
# folding/conv_1/weights/Adam (DT_FLOAT) [1,512,512]
# folding/conv_1/weights/Adam_1 (DT_FLOAT) [1,512,512]
# folding/conv_2/biases (DT_FLOAT) [3]
# folding/conv_2/biases/Adam (DT_FLOAT) [3]
# folding/conv_2/biases/Adam_1 (DT_FLOAT) [3]
# folding/conv_2/weights (DT_FLOAT) [1,512,3]
# folding/conv_2/weights/Adam (DT_FLOAT) [1,512,3]
# folding/conv_2/weights/Adam_1 (DT_FLOAT) [1,512,3]

# global_step (DT_INT32) []