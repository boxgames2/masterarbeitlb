import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import modelloader
from pcn import PCN
from open3d_util import plot_pcd, show_pcd
from io_util import read_pcd, save_pcd

def point_maxpool(inputs, npts, keepdims=False):
    outputs = [tf.reduce_max(f, axis=1, keepdims=keepdims) for f in tf.split(inputs, npts, axis=1)]
    return tf.concat(outputs, axis=0)

def create_plots(partial, complete):
    fig = plt.figure(figsize=(8, 4))
    ax = fig.add_subplot(121, projection='3d')
    plot_pcd(ax, partial)
    ax.set_title('Input')
    ax = fig.add_subplot(122, projection='3d')
    plot_pcd(ax, complete)
    ax.set_title('Output')
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0)
    
def test_pcn(pcn):
    #partial = read_pcd("./demo_data/airplane.pcd")
    #partial = read_pcd("./0000000002.txt")
    partial = read_pcd("./demo_data/P_a1copyRoom1_0.ply")
    pointcloud1 = partial
    #pointcloud1 = np.array([[0,0,0],[1,1,1],[2,2,2]])
    data = pointcloud1
    data = np.expand_dims(data, axis=0)
    data = data.astype(np.float32)
    pointcloud_shapes = [pointcloud1.shape[0]]
    
    f, fine, coarse = pcn(inputs=data, npts=pointcloud_shapes)
    fine = np.squeeze(fine)
    create_plots(partial, fine)

    show_pcd(partial)
    show_pcd(fine)
    plt.show()
  
def main1():
    model = tf.keras.Sequential()
    x = tf.keras.Input(shape=(None,3), batch_size=1, dtype=tf.float32, ragged=True)

    model.add(x)

    p1 = np.zeros((10, 3))
    p2 = np.zeros((12, 3))

    data = [p1.tolist(), p2.tolist()]

    x = tf.ragged.constant(data)

    #npts = [p1.shape[0], p2.shape[0]]

    #data = tf.concat([p1, p2], axis=0)
    #data = tf.expand_dims(data, axis=0)
    #print(data, npts)

    y = model(x)
    #print(y[0].to_tensor())
    print(y.shape)


def main2():
    x = tf.keras.Input(type_spec=tf.RaggedTensorSpec(shape=[None, None], dtype=tf.float32, ragged_rank=1), ragged=True)
    inpt_ts = []
    npts = []
    for i in range(x.shape[0]):
        xi_t = x[i].to_tensor()
        inpt_ts.append(xi_t)
        npts.append(xi_t.shape[0])

    x_ = tf.concat(inpt_ts, axis=0)
    x_ = tf.expand_dims(x_, axis=0)
    y = point_maxpool(inputs=x_, npts=npts)

    model = tf.keras.Model(inputs=x, outputs=y)


def main3():
    # model
    pcn = PCN(n_coarse=1024,
            grid_size=4,
            e_l1=[128,256],
            e_l2=[512,1024],
            d_l1=[1024,1024],
            d_l1_acts=[tf.nn.relu, tf.nn.relu],
            d_l2=[512,512,3])

    # data
    p1 = np.random.rand(10, 3)
    p2 = np.random.rand(12, 3)
    npts = [p1.shape[0], p2.shape[0]]
    print(npts)

    # this transformation could also be done in the pcn code?
    data = np.concatenate((p1, p2))
    data = np.expand_dims(data, axis=0)
    data = data.astype(np.float32)
    print(data.shape, data.dtype)

    #empty call to initiate the weights
    pcn(inputs=data, npts=npts)

    #set weights
    modelloader.load_weights_and_biases(pcn)
    
    test_pcn(pcn)
    
    """
    optimizer = tf.optimizers.Adam(learning_rate=0.001)

    # this could be an iteration of the training loop
    with tf.GradientTape() as tape:
        # pcn calculation
        feat, fine, coarse = pcn(inputs=data, npts=npts)
        
        # loss
        l1 = tf.reduce_max(feat)
        l2 = tf.reduce_max(fine)
        l3 = tf.reduce_max(coarse)

        loss = l1 + l2 + l3
        print("loss: {0}".format(loss))

        vars_ = tape.watched_variables()
        print("watched vars")
        print(vars_)
        grads = tape.gradient(loss, vars_)
        optimizer.apply_gradients(zip(grads, vars_))
    """


if __name__ == "__main__":
    main3()