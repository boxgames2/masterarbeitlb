import tensorflow as tf
import numpy as np
from matplotlib import pyplot as plt
import modelloader
from pcn import PCN
from open3d_util import prepare_with_bounding_box ,plot_pcd, show_pcd, prepare_pcd_data_without_bbox, rotate_pointcloud, transform_pointcloud, custom_draw_geometry_with_key_callback, get_diagonal_size, rescale_final
from io_util import read_pcd, save_pcd, get_float_input
import sys, getopt

tensor_type = "float16"
epochs = 20
training_set = 50
test_set = 10
distortion = 0.6

def create_plots(partial, complete):
    fig = plt.figure(figsize=(8, 4))
    ax = fig.add_subplot(121, projection='3d')
    plot_pcd(ax, partial)
    ax.set_title('Input')
    ax = fig.add_subplot(122, projection='3d')
    plot_pcd(ax, complete)
    ax.set_title('Output')
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0)
 
def use_pcn(pcn, partial, bbox_path = None):
    initial_diagonal = get_diagonal_size(partial)
    print("initial size = ",initial_diagonal)
    
    i = ""
    #in case a boundingbox is provided, use it to prepare the data.
    if bbox_path : 
        partial = prepare_with_bounding_box(partial,bbox_path)
         
    while i != "c":
        partial = custom_draw_geometry_with_key_callback(partial, pcn)
        i = input("Rotate axis [rx,ry,rz] | scale [s] | Transform axis [tx,ty,tz] | Automated adjust without given bbox [a] | Save Prediction[sc] | Save Input[si] | Continue [c]")
        i = i.strip()
        if i == "a":
            partial = prepare_pcd_data_without_bbox(partial)        
        elif i == "sc":
            fine = pcn.predict(partial)
            fine = rescale_final(fine, initial_diagonal)
            show_pcd(fine)
            filename = input("Path and Filename [./example]")
            save_pcd(filename+".pcd", fine) 
            print("File Saved as ",filename,".pcd")        
        elif i == "si":
            filename = input("Path and Filename [./example]")
            save_pcd(filename+".pcd", partial)                  
        elif i == "s":
            scale = get_float_input("Scale factor [float]")
            if scale == "c":
                scale = 1
            partial /= float(scale)        
        elif i in ["rx","ry","rz"]:
            i = i[1]
            #get the angle as a rad float value and rotate the 
            angle = get_float_input("Rotation angle [float]")
            if angle == "c":
                angle = 0 
            partial = rotate_pointcloud(partial, i, angle)         
        elif i in ["tx","ty","tz"]:
            i = i[1]
            #get the angle as a rad float value and rotate the 
            transformation = get_float_input("transformation value [float]")
            if transformation == "c":
                transformation = 0  
            partial = transform_pointcloud(partial, i, transformation)
        elif i == "c":
            return      
        else:
            print("unknown input")
            pass
  
def main(model, bbox):
    model_pcd = read_pcd(model)
    print(model_pcd)
    pcn = PCN(n_coarse=1024,
            grid_size=4,
            e_l1=[128,256],
            e_l2=[512,1024],
            d_l1=[1024,1024],
            d_l1_acts=[tf.nn.relu, tf.nn.relu],
            d_l2=[512,512,3])
    #set weights
    modelloader.load_weights_and_biases(pcn)
    use_pcn(pcn, model_pcd, bbox)
    
#python main.py -m "./ArtificialData/sampled_halfChair.pcd" -b "./ArtificialData/ChairBbox_sampled.pcd"
#python main.py -m "./ArtificialData/chair_1.pcd" -b "./ArtificialData/chair_1_bbox.pcd"
if __name__ == "__main__":
    argv = sys.argv[1:]
    model = ""
    bbox = ""
    try:
      opts, args = getopt.getopt(argv,"m:b:",["model=","bbox="])
    except getopt.GetoptError:
        print ('main.py -m <model> -b <boundingbox>')
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-m", "--model"):
            model = arg
        elif opt in ("-b", "--bbox"):
            bbox = arg
    main(model, bbox)
    