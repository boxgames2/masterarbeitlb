import tensorflow as tf
import pickle
import json
import numpy as np
from matplotlib import pyplot as plt
import modelloader
from pcn import PCN
from open3d_util import prepare_with_bounding_box ,plot_pcd, show_pcd, prepare_pcd_data_without_bbox, rotate_pointcloud, transform_pointcloud, custom_draw_geometry_with_key_callback, get_diagonal_size, rescale_final
from io_util import read_pcd, save_pcd, get_float_input
from ArtificialData.artificial_data_util import create_train_transform_dataset
import datetime
import os

tensor_type = "float16"
epochs = 20
training_set = 50
test_set = 10
distortion = 0.6
def create_plots(partial, complete):
    fig = plt.figure(figsize=(8, 4))
    ax = fig.add_subplot(121, projection='3d')
    plot_pcd(ax, partial)
    ax.set_title('Input')
    ax = fig.add_subplot(122, projection='3d')
    plot_pcd(ax, complete)
    ax.set_title('Output')
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1, wspace=0)
 
def use_pcn(pcn, partial, ground_truth):
    initial_diagonal = get_diagonal_size(partial)
    print("size = ",initial_diagonal)
    #partial = prepare_pcd_data(partial)
    i = ""
    prepare_with_bounding_box(partial,"./ArtificialData/ChairBbox_sampled")
    while i != "c":
        partial = custom_draw_geometry_with_key_callback(partial, pcn)
        i = input("Rotate axis [rx,ry,rz] | scale [s] | Transform axis [tx,ty,tz] | Automated adjust [a] | Save Prediction[sc] | Save Input[si] | Test Loss[l]| Continue [c]")
        i = i.strip()
        if i == "a":
            partial = prepare_pcd_data_without_bbox(partial)        
        elif i == "sc":
            fine = pcn.predict(partial)
            fine = rescale_final(fine, initial_diagonal)
            show_pcd(fine)
            filename = input("Path and Filename [./example]")
            save_pcd(filename+".pcd", fine)
            
        elif i == "si":
            filename = input("Path and Filename [./example]")
            save_pcd(filename+".pcd", partial)
                  
        elif i == "s":
            scale = get_float_input("Scale factor [float]")
            if scale == "c":
                scale = 1
            partial /= float(scale)        
        elif i in ["rx","ry","rz"]:
            i = i[1]
            #get the angle as a rad float value and rotate the 
            angle = get_float_input("Rotation angle [float]")
            if angle == "c":
                angle = 0 
            partial = rotate_pointcloud(partial, i, angle)         
        elif i in ["tx","ty","tz"]:
            i = i[1]
            #get the angle as a rad float value and rotate the 
            transformation = get_float_input("transformation value [float]")
            if transformation == "c":
                transformation = 0  
            partial = transform_pointcloud(partial, i, transformation)
        elif i == "l":
            fine = pcn.predict(partial)
            fine = rescale_final(fine, initial_diagonal)
            print(fine.shape)
            pcn.calc_cd_loss(tf.cast(fine, tensor_type), tf.cast(ground_truth, tensor_type))
        elif i == "c":
            return      
        else:
            print("unknown input")
            pass
  
def main3():
    #partial = read_pcd("./demo_data/airplane.pcd")
    #partial = read_pcd("./demo_data/chair_01.pcd")
    #partial = read_pcd("./demo_data/airplane.pcd")
    ground_truth = read_pcd("./ArtificialData/prepared_full_sofa.pcd")
    partial = read_pcd("./ArtificialData/prepared_half_sofa.pcd")
    chair_ground_truth = read_pcd("./ArtificialData/sampled_fullChair.pcd")
    chair_partial = read_pcd("./ArtificialData/sampled_halfChair.pcd")
    #partial = read_pcd("./demo_data/P_a1copyRoom1_0.ply")
    #partial = read_pcd("./demo_data/chair_1.txt","txt")
    #partial = read_pcd("./demo_data/table_2.txt","txt")
    
    # model
    #os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    pcn = PCN(n_coarse=1024,
            grid_size=4,
            e_l1=[128,256],
            e_l2=[512,1024],
            d_l1=[1024,1024],
            d_l1_acts=[tf.nn.relu, tf.nn.relu],
            d_l2=[512,512,3])

    #set weights
    modelloader.load_weights_and_biases(pcn)
    use_pcn(pcn, chair_partial, ground_truth)
    
    #training preperation
    #labels = []
    #test_labels = []
    # dataset, pointcloud_shapes = create_train_transform_dataset(partial, int(training_set/2), distortion)
    # test_dataset, test_pointcloud_shapes = create_train_transform_dataset(partial, test_set, distortion)
    # for _ in dataset:
    #     labels.append(ground_truth)
    # for _ in test_dataset:
    #     test_labels.append(ground_truth)
        
    # chair_dataset, chair_pointcloud_shapes = create_train_transform_dataset(chair_partial, int(training_set/2), distortion)
    # chair_test_dataset, chair_test_pointcloud_shapes = create_train_transform_dataset(partial, test_set, distortion)
    # for _ in chair_dataset:
    #     labels.append(chair_ground_truth)
    # for _ in chair_test_dataset:
    #     test_labels.append(chair_ground_truth)
    # dataset = np.concatenate((dataset, chair_dataset))
    # test_dataset = np.concatenate((test_dataset, chair_test_dataset))
    
    # rnd_order = np.random.permutation(len(dataset))
    # dataset = np.array(dataset)
    # labels = np.array(labels)
    # data, labels = dataset[rnd_order], labels[rnd_order]

    # train_pcn(pcn, data, labels, pointcloud_shapes, test_dataset, test_labels)

def train_pcn(pcn, data, labels, pointcloud_shapes, test_dataset, test_labels):
    
    ##########BEFORE TRAINING###################
    current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    # directory where the logs should be saved
    log_dir = "./logs/tf/" + current_time
    train_summary_writer = tf.summary.create_file_writer(log_dir)

    train_index = 0
    test_step = 0
    
    save_path = 'trained_model_weight_and_bias_data_distortion06.pkl'
    for epoch in range(epochs):
        print("epoch:",epoch)
        for idx, cloud in enumerate(data):
            train_index += 1
            print(train_index,":")
            loss = optimize(pcn, cloud=cloud, pointcloud_shapes=pointcloud_shapes, ground_truth=labels[idx]) 
            loss = np.asarray(loss).item()
            with train_summary_writer.as_default():
                tf.summary.scalar("train/loss", loss, step=train_index)
 
            
        #if train_index%5 ==0:
        test_step+=1
        test_loss = (calc_avg_loss(test_dataset,test_labels,pcn))
        test_loss = np.asarray(test_loss).item()    
        with train_summary_writer.as_default():
            tf.summary.scalar("test/loss", test_loss, step=test_step)
            train_summary_writer.flush()
    modelloader.save_weights_and_biases(pcn, save_path)
    
    
def calc_avg_loss(test_dataset,test_labels,pcn):
    test_index = 0
    loss = 0
    for idx,test_cloud in enumerate(test_dataset):
        test_index+=1
        data = test_cloud
        data = tf.expand_dims(data, axis=0)
        data = tf.cast(data,tf.float32)
        #usint only shape 0, since the input is just one cloud
        pointcloud_shapes = [test_cloud.shape[0]]
        _, fine, _ = pcn(data, pointcloud_shapes)
        loss+= pcn.calc_cd_loss(tf.cast(fine, tensor_type), tf.cast(test_labels[idx], tensor_type))
    return loss/test_index
            
def optimize(pcn, cloud, pointcloud_shapes, ground_truth):
    #TODO: add tensorboard
    optimizer = tf.optimizers.Adam(learning_rate=0.001)
    #create a tape to record used tensor weights.
    with tf.GradientTape(watch_accessed_variables=True) as tape:
        #calc the loss of a prediction
        data = cloud
        data = tf.expand_dims(data, axis=0)
        data = tf.cast(data,tf.float32)
        #usint only shape 0, since the input is just one cloud
        pointcloud_shapes = [cloud.shape[0]]
        _, fine, _ = pcn(data, pointcloud_shapes)
        #fine = pcn.predict(cloud,pointcloud_shapes)
        loss = pcn.calc_cd_loss(tf.cast(fine, tensor_type), tf.cast(ground_truth, tensor_type))
        print("loss: {0}".format(loss))

        vars_ = tape.watched_variables()
        trainable = 0
        for v in vars_:
            if v.trainable:
                trainable+=1
        print("trainable vars = :",trainable)
        grads = tape.gradient(loss, vars_)
        #print(grads)
        optimizer.apply_gradients(zip(grads, vars_))
    return loss
    
if __name__ == "__main__":
    #sys.setrecursionlimit(1500)
    # gpu = False
    # if not gpu:
    #         os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
    main3()
    