from MLP1DCONV import MLP1DCONV
from PointPooling import DepthMaxPool
import tensorflow as tf
from tensorflow.keras.layers import MaxPooling1D
'''
Ich versuche im Testscript zunächst den Encoder des PCN_cd(z.47-55) nachzubauen.
Vorlage hierfür ist im pcn repo: ./models/pcn_cd.py.

def create_encoder(self, inputs, npts):
    with tf.variable_scope('encoder_0', reuse=tf.AUTO_REUSE):
        features = mlp_conv(inputs, [128, 256])
        features_global = point_unpool(point_maxpool(features, npts, keepdims=True), npts)
        features = tf.concat([features, features_global], axis=2)
    with tf.variable_scope('encoder_1', reuse=tf.AUTO_REUSE):
        features = mlp_conv(features, [512, 1024])
        features = point_maxpool(features, npts)
    return features

def create_decoder(self, features):
    with tf.variable_scope('decoder', reuse=tf.AUTO_REUSE):
        coarse = mlp(features, [1024, 1024, self.num_coarse * 3])
        coarse = tf.reshape(coarse, [-1, self.num_coarse, 3])

Dieser besteht aus einer MLP_conv Struktur (pcn repo ./tf_utils.py),
gefolgt con einem Pointmaxpooling innerhalb eines point_unpooling.
Anschließend eine weitere MLP_conv struktur und ein Point_Maxpooling.
'''
#Variablen für den Decoder. Noch unwichtig
num_coarse = 1024
grid_size = 4
grid_scale = 0.05
num_fine = grid_size ** 2 * num_coarse

#Erstellen der beiden mlp_conv Layer.
mlp = MLP1DCONV(layer_dims=[128,256])
mlp2 = MLP1DCONV(layer_dims=[512, 1024])

#Geplant ist es das Model tf2 gerecht zu einem keras.Sequential umzubauen
model = tf.keras.Sequential()

#Encoder
model.add(mlp)
#Die funktionsweise/ziel des Point_maxpoolings ist mir noch etwas Unklar. Hätte das mlp den output (3,256) ist die Konvertierung zu (3,1) naheliegend.
#hier ist jedoch unklar wieso in PCN die npts benötigt werden. Zusätzlich hat der Output die shape (1, 256).
#model.add( tf.keras.layers.MaxPool1D(pool_size=128, data_format='channels_first'))
#Versuch der Umsetzung als custom Layer
#n_points=2
#model.add(DepthMaxPool(n_points=n_points))


#Zweite Hälfte des Encooders
#model.add(mlp2)
#model.add(DepthMaxPool(n_points=n_points))#todo maxpooling

#Decoder
#...

#Da noch kein Inputlayer Existier, muss ein Inputlayer mit der gewünschten Shape erstellt werden. Abhängig von der Pointcloud?
model.build([None, 2, 3])

print(model.summary())


#Zum Verständnis der Layer, habe ich diese noch mal einzeln geadded und lasse mir die summary ausgeben.
model2 = tf.keras.Sequential()
layer_dims=[128,256]
for layer_dim in layer_dims[:-1]:
    c1d = tf.keras.layers.Conv1D(filters=layer_dim, kernel_size=1, activation=tf.nn.relu)
    model2.add(c1d)
c1d = tf.keras.layers.Conv1D(filters=layer_dims[-1], kernel_size=1, activation=None)
model2.add(c1d)
model2.build([None, 2, 3])

print(model2.summary())
test = [[[0,0,0],[1,1,1]]]
model.compile(optimizer="Adam", loss="mse", metrics=["mae"])
pred = model.predict(test)
print(pred.shape)
print(pred)