import tensorflow as tf
import numpy as np

from tf_util import evaluate as cd_loss
from mlp1dconv import MLP1DCONV
from mlp import MLP

def point_maxpool(inputs, npts, keepdims=False):
    outputs = [tf.reduce_max(f, axis=1, keepdims=keepdims) for f in tf.split(inputs, npts, axis=1)]
    outputs = tf.concat(outputs, axis=0)
    return outputs


def point_unpool(inputs, npts):
    inputs = tf.split(inputs, inputs.shape[0], axis=0)
    outputs = [tf.tile(f, [1, npts[i], 1]) for i,f in enumerate(inputs)]
    return tf.concat(outputs, axis=1)


class PCN():   
    def __init__(
            self,
            n_coarse=1024,
            grid_size=4,
            e_l1=[128,256],
            e_l2=[512,1024],
            d_l1=[1024,1024],
            d_l1_acts=[tf.nn.relu, tf.nn.relu],
            d_l2=[512,512,3]
            ):
        if len(d_l1) != len(d_l1_acts):
            raise Exception("layers and activation of mlp mismatch")
        self.n_coarse = n_coarse
        self.grid_size = grid_size
        self.n_fine = grid_size ** 2 * n_coarse
        self.e_mlp1 = MLP1DCONV(layer_dims=e_l1, name="e_mlp1")
        self.e_mlp1.layers[0].trainable = False
        self.e_mlp2 = MLP1DCONV(layer_dims=e_l2, name="e_mlp2")
        self.e_mlp2.layers[0].trainable = False
        d_l1.append(n_coarse * 3)
        self.d_mlp1 = MLP(layer_dims=d_l1, activations=d_l1_acts, name="d_mlp1")
        self.d_mlp2 = MLP1DCONV(layer_dims=d_l2, name="d_mlp2")
        self.__init_weights()
        
    def __init_weights(self):
        p1 = np.random.rand(10, 3)
        p2 = np.random.rand(12, 3)
        npts = [p1.shape[0], p2.shape[0]]
        # this transformation could also be done in the pcn code?
        data = np.concatenate((p1, p2))
        data = np.expand_dims(data, axis=0)
        data = data.astype(np.float32)
        #print(data.shape, data.dtype)
        #empty call to initiate the weights
        self.e_mlp1.layers[0].trainable = False
        self.e_mlp2.layers[0].trainable = False
        self(inputs=data, npts=npts)
        
    def get_e_mlp1(self):
        return self.e_mlp1
    
    def get_e_mlp2(self):
        return self.e_mlp2
    
    def get_d_mlp1(self):
        return self.d_mlp1
    
    def get_d_mlp2(self):
        return self.d_mlp2
    
    def set_e_mlp1(self, mlp):
        self.e_mlp1 = mlp
    
    def set_e_mlp2(self, mlp):
        self.e_mlp2 = mlp
    
    def set_d_mlp1(self, mlp):
        self.d_mlp1 = mlp
    
    def set_d_mlp2(self, mlp):
        self.d_mlp2 = mlp
    
    def encoder(self, inputs, npts):
        # encoder 0
        f = self.e_mlp1(inputs)     
        fg = point_maxpool(inputs=f, npts=npts, keepdims=True)
        fg = point_unpool(inputs=fg, npts=npts)
        f = tf.concat([f, fg], axis=2)
        # encoder 1
        f = self.e_mlp2(f)
        f = point_maxpool(inputs=f, npts=npts)    
        return f
    
    def decoder(self, f):
        # decoder
        coarse = self.d_mlp1(f)
        coarse = tf.reshape(coarse, [-1, self.n_coarse, 3])
        
        # folding
        grid = tf.meshgrid(tf.linspace(-0.05, 0.05, self.grid_size), tf.linspace(-0.05, 0.05, self.grid_size))
        grid = tf.expand_dims(tf.reshape(tf.stack(grid, axis=2), [-1, 2]), 0)
        grid_feat = tf.tile(grid, [f.shape[0], self.n_coarse, 1])

        point_feat = tf.tile(tf.expand_dims(coarse, 2), [1, 1, self.grid_size ** 2, 1])
        point_feat = tf.reshape(point_feat, [-1, self.n_fine, 3])

        global_feat = tf.tile(tf.expand_dims(f, 1), [1, self.n_fine, 1])

        feat = tf.concat([grid_feat, point_feat, global_feat], axis=2)

        center = tf.tile(tf.expand_dims(coarse, 2), [1, 1, self.grid_size ** 2, 1])
        center = tf.reshape(center, [-1, self.n_fine, 3])

        fine = self.d_mlp2(feat) + center
        return fine, coarse
    
    def predict(self, partial):
        #adjustable to work with batches?
        data = partial
        data = np.expand_dims(data, axis=0)
        data = data.astype(np.float32)
        pointcloud_shapes = [partial.shape[0]]
        
        _, fine, _ = self(inputs=data, npts=pointcloud_shapes)
        fine = np.squeeze(fine) 
        writable_fine = np.copy(fine) 
        writable_fine.setflags(write=1) 
        return writable_fine
    
    def predict_batch(self, data, pointcloud_shapes):
        _, fine, _ = self(inputs=data, npts=pointcloud_shapes)
        fine = np.squeeze(fine) 
        writable_fine = np.copy(fine) 
        writable_fine.setflags(write=1) 
        return writable_fine
    
    def test_evaluate(self, fine, gt):
        fine = fine[16000,:]
        mse = tf.reduce_mean((fine-gt)**2)
        return mse
        
    def calc_cd_loss(self, prediction, ground_truth):
        #loss=self.test_evaluate(prediction,ground_truth)
        loss = cd_loss(prediction, ground_truth)
        return loss
  
    def __call__(self, inputs, npts):
        f = self.encoder(inputs=inputs, npts=npts)
        fine, coarse = self.decoder(f=f)
        #f = 1, 1024
        #fine = np.zeros((1, 16384, 3))
        return f, fine, coarse