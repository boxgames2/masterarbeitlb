import tensorflow as tf


class MLP1DCONV(tf.keras.layers.Layer):
    def __init__(self, layer_dims, name=None):
        super(MLP1DCONV, self).__init__()
        self.layers = []
        if name is None:
            name = ""
        else:
            name += "_"
        for layer_dim, i in zip(layer_dims[:-1], range(len(layer_dims)-1)):
            print("Dimension: ",layer_dim)
            c1d = tf.keras.layers.Conv1D(
                filters=layer_dim,
                kernel_size=1,
                activation=tf.nn.relu,
                name=name + "c1d_" + str(i),trainable = True)
            self.layers.append(c1d)
        c1d = tf.keras.layers.Conv1D(
            filters=layer_dims[-1],
            kernel_size=1,
            activation=None,
            name=name + "c1d_" + str(len(layer_dims)-1),trainable = True)
        self.layers.append(c1d)

    def set_layer_weights(self,layer_index, weights, biases):
        """Sets the biases and weights of a specific intern layer.

        Args:
            layer_index ([type]): [description]
            weights ([type]): [description]
            biases ([type]): [description]
        """
        layer = self.layers[layer_index]
        layer.set_weights([weights,biases])
        
        
    def get_layer_weights(self,layer_index):
        """gets the biases and weights of a specific intern layer.

        Args:
            layer_index ([type]): [description]
            weights ([type]): [description]
            biases ([type]): [description]
        """
        layer = self.layers[layer_index]
        weights = layer.get_weights()[0]
        biases = layer.get_weights()[1]
        return weights, biases
        
    def build(self, input_shape):
        pass

    def call(self, inputs):
        for layer in self.layers:
            inputs = layer(inputs)
        return inputs