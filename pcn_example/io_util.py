from fnmatch import translate
import numpy as np
import open3d as o3d
import lmdb
from os.path import exists

def read_pcd(filename,format=""):
    if format == "txt":
        pcd = o3d.io.read_point_cloud(filename, format="xyzrgb")
    else:
        pcd = o3d.io.read_point_cloud(filename)
    return np.array(pcd.points)
        
def save_pcd(filename, points):
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    o3d.io.write_point_cloud(filename, pcd)
    
def get_float_input(infostring = "Value [float] | Continue [c]", skip_char = 'c'):
    """Returns a userinput as float if possible

    Args:
        infostring (str, optional): String that is shown to the user to provide input "Value [float] | Continue [c]".
        skip_char (str, optional): character that is returned if conversion not possible. Defaults to 'c'.

    Returns:
        _type_: _description_
    """
    number = input(infostring)
    if number == skip_char:
        return skip_char
    try:   
        float(number)             
    except Exception as e:
        return skip_char
    return number

def convert_txt_to_pcd():
    pcd = read_pcd("./ArtificialData/chair_1.txt", "txt")
    save_pcd("./ArtificialData/chair_1.pcd", pcd)          
            
if __name__ == "__main__":
    convert_txt_to_pcd()