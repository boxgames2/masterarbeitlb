import tensorflow as tf

from mlp1dconv import MLP1DCONV

class Encoder(tf.keras.layers.Layer):
    def __init__(self, name=None):
        super(Encoder, self).__init__()

    def build(self, input_shape):
        #print(input_shape)
        self.mlp1 = MLP1DCONV(layer_dims=[128, 256])

    def call(self, inputs):
        x = self.mlp1(inputs)
        return x