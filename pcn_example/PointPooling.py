import tensorflow as tf

class DepthMaxPool(tf.keras.layers.Layer):
    def __init__(self, n_points):
        super().__init__()
        self.n_points = n_points
        
    def call(self, inputs):
        outputs = [tf.reduce_max(f, axis=1, keepdims=False)
        for f in tf.split(inputs, self.n_points, axis=1)]
        return tf.concat(outputs, axis=0)
    
    
    
class DepthUnPool(tf.keras.layers.Layer):
    def __init__(self, n_points):
        super().__init__()
        self.n_points = n_points
        
    def call(self, inputs):
        inputs = tf.split(inputs, inputs.shape[0], axis=0)
        outputs = [tf.tile(f, [1, self.n_points[i], 1]) for i,f in enumerate(inputs)]
        return tf.concat(outputs, axis=1)