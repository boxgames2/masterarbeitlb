from glob import glob
from logging.handlers import RotatingFileHandler
import numpy as np
import open3d as o3d
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pcn import PCN
from io_util import read_pcd

def plot_pcd(ax, pcd):
    ax.scatter(pcd[:, 0], pcd[:, 1], pcd[:, 2], zdir='y', s=0.5, cmap='Reds', vmin=-1, vmax=0.5)
    ax.set_axis_off()
    ax.set_xlim(-0.3, 0.3)
    ax.set_ylim(-0.3, 0.3)
    ax.set_zlim(-0.3, 0.3)

def show_pcd(points, bbox = None):
    pcd = o3d.geometry.PointCloud()
    points = np.squeeze(points)
    pcd.points = o3d.utility.Vector3dVector(points)
    if not bbox:
        o3d.visualization.draw_geometries([pcd,coordinate_system(), o3d.geometry.OrientedBoundingBox.create_from_points(pcd.points)])
    else:
        o3d.visualization.draw_geometries([pcd,coordinate_system(), bbox])

def coordinate_system():
    line_set = o3d.geometry.LineSet()
    points = np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]])
    colors = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    lines = np.array([[0, 1], [0, 2], [0, 3]]).astype(int)
    line_set.points = o3d.utility.Vector3dVector(points)
    line_set.colors = o3d.utility.Vector3dVector(colors)
    line_set.lines = o3d.utility.Vector2iVector(lines)
    return line_set

def rotate_pointcloud(pcd, axis, angle):
    """_summary_
        rotate a given Pointcloud around an axis by a given angle. Returns therotated cloud.
    Args:
        pcd (_type_): pointcloud to rotate
        axis (_type_): rotation axis
        angle (_type_): angle in degree

    Returns:
        pointcloud: rotated pointcloud
    """
    try:
        angle = float(angle)/180 * np.pi
    except ValueError:
        return pcd
    if axis == "x":     
        rotation = np.array([
            [1, 0, 0],
            [0, np.cos(angle), -np.sin(angle)],
            [0, np.sin(angle), np.cos(angle)]])
    if axis == "y":     
        rotation = np.array([[np.cos(angle), 0, np.sin(angle)],
                [0, 1, 0],
                [-np.sin(angle), 0, np.cos(angle)]])
    if axis == "z":
        rotation = np.array([[np.cos(angle), -np.sin(angle), 0],
                    [np.sin(angle), np.cos(angle), 0],
                    [0, 0, 1]])
    pcd = np.dot(pcd, rotation)
    return pcd

def transform_pointcloud(_pcd, axis, value):
    pcd = _pcd
    try:
        value = float(value)
    except ValueError:
        return pcd
    transformation = [0,0,0]
    if axis == "x":     
        transformation = [value , 0 , 0]
    if axis == "y":     
        transformation = [0 , value , 0]
    if axis == "z":
        transformation = [0 , 0 , value]
    pcd += transformation
    
    return pcd

def scale_pointcloud(pcd_array, factor):
    """
    scales the Pointcloud by dividing
    Args:
        pcd (_type_): _description_
        factor (_type_): _description_
    """
    return pcd_array/float(factor)

def prepare_with_bounding_box(pcd, bounding_box_path):
    bounding_box_path_cloud = bounding_box_path
    # bounding_box_path_info = bounding_box_path+".txt"
    # #currently takes always the positive direction
    # with open(bounding_box_path_info) as f:
    #     lines = f.readlines()
    #     up = lines[0]
    #     front = lines[1]
    # print("up : " ,up)
    # print("front : ", front)
    bbox  =read_pcd(bounding_box_path_cloud)
    scale = _find_diagonal_distance(bbox)
    bbox = scale_pointcloud(bbox, scale)
    pcd = scale_pointcloud(pcd, scale)
    
    #convert bbox array to OrientedBoundingBox, used for the center operation
    bbox = o3d.utility.Vector3dVector(bbox)
    bbox = o3d.geometry.OrientedBoundingBox.create_from_points(bbox)
    
    #translate the boundingbox to fit the translated pcd
    center = bbox.get_center()
    bbox.translate(-center)
    pcd -= center
    
    #Rotate:
    
    
    pcd = o3d.utility.Vector3dVector(pcd)
    return pcd
    
def rotate_to_open3d_up_vector(pcd, axis):
    print("axis: ", axis)
    if axis == "x": 
        print("rotating x") 
        pcd = rotate_pointcloud(pcd=pcd, angle=90, axis="z")
    if axis == "y":
        print("rotating y") 
    if axis == "z":
        print("rotating z") 
        pcd = rotate_pointcloud(pcd=pcd, angle=90, axis="x")  
    return pcd

def prepare_pcd_data_without_bbox(pcd, bounding_box_path = None):
    #create boundingbox from input pcd
    if not bounding_box_path:
        bbox_points =o3d.utility.Vector3dVector(pcd)
        bbox = o3d.geometry.OrientedBoundingBox.create_from_points(bbox_points)

    # Calculate center, rotation and scale
    center = bbox.get_center()
    
    #translate the boundingbox to fit the translated pcd
    bbox.translate(-center)
    
    #convert the bbox to points to use dot operations
    bbox = np.asarray(bbox.get_box_points())
    
    #get the Rotation ob the bboc based on its up-vector
    yaw = np.arctan2(bbox[2, 1] - bbox[7, 1], bbox[2, 0] - bbox[7, 0])
    rotation = np.array([[np.cos(yaw), 0, np.sin(yaw)],
                         [0, 1, 0],
                         [-np.sin(yaw), 0, np.cos(yaw)]])
    #rotate the bbox before calculating the scale
    bbox = np.dot(bbox, rotation) 
    
    #manipulate the pointcloud
    pcd -= center
    pcd = np.matmul(pcd, rotation)

    return pcd

def _find_diagonal_distance(bbox):
    dist = 0
    for point1 in bbox:
        for point2 in bbox:
            temp = np.linalg.norm(point1 - point2)
            if temp > dist:
                dist = temp
    return dist
    
def get_diagonal_size(pcd):
    bbox_points =o3d.utility.Vector3dVector(pcd)
    bbox = o3d.geometry.OrientedBoundingBox.create_from_points(bbox_points) 
    bbox = np.asarray(bbox.get_box_points())
    return _find_diagonal_distance(bbox)

def rescale_final(final, initial_diagonal):
    final_bbox_points =o3d.utility.Vector3dVector(final)
    final_bbox = o3d.geometry.OrientedBoundingBox.create_from_points(final_bbox_points) 
    final_bbox = np.asarray(final_bbox.get_box_points())
    final_diagonal = _find_diagonal_distance(final_bbox)
    scale_factor = initial_diagonal/final_diagonal
    return final * scale_factor
    

direction = 1
def custom_draw_geometry_with_key_callback(pcd,pcn):
    """Starts a window with a transformable Pointcloud wich can be transformated.
    Controls:
    Translate:  1, 2, 3
    Rotate:     4, 5, 6
    Scale:      A, S 
    Change directions: W
    
    Args:
        pcd (np_array[x,3]): Pointcloud wich schould be reconstruckted using PCN
        pcn (pcn model): Trained PCN Model

    Returns:
        _type_: Fine Pointcloud after the Prediction with PCN
    """
    pcd_o3d = o3d.geometry.PointCloud()
    pcd_o3d.points = o3d.utility.Vector3dVector(pcd)
    fine = o3d.geometry.PointCloud()
    
    def toggle_direction(vis):
        global direction
        print("direction changed")
        direction *= -1
        
    def translate_x(vis):
        P = np.asarray(pcd_o3d.points)
        P = transform_pointcloud(P, "x", 0.005*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
    def translate_y(vis):
        P = np.asarray(pcd_o3d.points)
        P = transform_pointcloud(P, "y", 0.005*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
    def translate_z(vis):
        P = np.asarray(pcd_o3d.points)
        P = transform_pointcloud(P, "z", 0.005*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
        
    def rotate_x(vis):
        P = np.asarray(pcd_o3d.points)
        P = rotate_pointcloud(P, "x", 1*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
        return False 
    def rotate_y(vis):
        P = np.asarray(pcd_o3d.points)
        P = rotate_pointcloud(P, "y", 1*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
    def rotate_z(vis):
        P = np.asarray(pcd_o3d.points)
        P = rotate_pointcloud(P, "z", 1*direction)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
    
    
    def scale_up(vis):
        P = np.asarray(pcd_o3d.points)
        P = scale_pointcloud(P, 0.98)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
    def scale_down(vis):
        P = np.asarray(pcd_o3d.points)
        P = scale_pointcloud(P, 1.02)
        pcd_o3d.points = o3d.utility.Vector3dVector(P)
        update_vis(vis)
        
    def update_vis(vis):
        fine_points = pcn.predict(np.asarray(pcd_o3d.points))
        fine_points = transform_pointcloud(fine_points,"x",1.5)
        fine.points = o3d.utility.Vector3dVector(fine_points)
        vis.update_geometry(fine)
        vis.update_geometry(pcd_o3d)
        
    key_to_callback = {}
    
    key_to_callback[ord("1")] = translate_x
    key_to_callback[ord("2")] = translate_y
    key_to_callback[ord("3")] = translate_z
    
    key_to_callback[ord("4")] = rotate_x
    key_to_callback[ord("5")] = rotate_y
    key_to_callback[ord("6")] = rotate_z
    
    key_to_callback[ord("W")] = toggle_direction
    key_to_callback[ord("A")] = scale_up
    key_to_callback[ord("S")] = scale_down
    
    fine_points = pcn.predict(np.asarray(pcd_o3d.points))
    fine_points = transform_pointcloud(fine_points,"x",1.5)
    fine.points = o3d.utility.Vector3dVector(fine_points)
    o3d.visualization.draw_geometries_with_key_callbacks([pcd_o3d, fine, coordinate_system()], key_to_callback)
    return np.array(pcd_o3d.points)