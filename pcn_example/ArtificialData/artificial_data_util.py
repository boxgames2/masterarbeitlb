
from open3d_util import transform_pointcloud
import random
import numpy as np
def create_train_transform_dataset(origin_cloud, data_size, max_distortion):
    """
        takes a given cloud and creates a dataset of randomly transfomrated clouds out of it. 
    Args:
        origin_cloud (open3D Pointcloud): Pointcloud to be transformed
        data_size (int) : desired count of artificial clouds
    Returns:
        data, n_points: returns the concatinated Date together with n_point array
    """
    npts = []
    data = None
    data_set = False
    directions=["x","y","z"]
    for i in range(data_size):
        direction = directions[random.randint(0, 2)]
        distortion = random.uniform(0,max_distortion)
        temp_cloud = np.array(origin_cloud, copy=True)
        cloud = transform_pointcloud(temp_cloud, direction, distortion)
        npts.append(cloud.shape[0])
        if data_set:
            data = np.concatenate((data,[cloud]))
        else:
            data = [cloud]
            data_set = True
    return data, npts

# def create_train_transform_dataset(origin_cloud, data_size, max_distortion):
#     """
#         takes a given cloud and creates a dataset of randomly transfomrated clouds out of it. 
#     Args:
#         origin_cloud (open3D Pointcloud): Pointcloud to be transformed
#         data_size (int) : desired count of artificial clouds
#     Returns:
#         data, n_points: returns the concatinated Date together with n_point array
#     """
#     npts = []
#     data = np.array(origin_cloud)
        
#     directions=["x","y","z"]
#     for i in range(data_size):
#         direction = directions[random.randint(0, 2)]
#         distortion = random.uniform(0,max_distortion)
#         cloud = transform_pointcloud(origin_cloud, direction, distortion)
#         npts.append(cloud.shape[0])
#         data = np.concatenate((data, cloud))
        
#     data = np.expand_dims(data, axis=0)
#     data = data.astype(np.float32)
#     return data, npts