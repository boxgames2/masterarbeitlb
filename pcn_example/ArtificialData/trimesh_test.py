import trimesh
import sys
sys.path.insert(0,'..')
from open3d_util import show_pcd
from io_util import save_pcd

def load_mesh_from_path(path):
    mesh = trimesh.load_mesh(path)
    pointcloud, _ = trimesh.sample.sample_surface(mesh,16000)
    return pointcloud

if __name__ == "__main__":
    pointcloud = load_mesh_from_path("fullChair.obj")
    save_pcd("sampled_fullChair.pcd", pointcloud)
    pointcloud = load_mesh_from_path("halfChair.obj")
    save_pcd("sampled_halfChair.pcd", pointcloud)
    show_pcd(pointcloud)