
from statistics import mode
import pickle
from turtle import shape
import numpy as np
from pcn import PCN

def load_weights_and_biases(pcn):
    weight_prefix="weights"
    bias_prefix = "biases"
    #model_weight_and_bias_data = open("model_weight_and_bias_data.pkl", "rb")
    model_weight_and_bias_data = open("trained_model_weight_and_bias_data_distortion03.pkl", "rb")
    model_weight_and_bias_dict = pickle.load(model_weight_and_bias_data)
    weights_to_assign = 0
    encoder_conv_mlp_to_assign = 0
    decoder_mlp_to_assign = 0
    folding_conv_mlp_to_assign = 0
    
    weights_assigned = 0
    encoder_conv_mlp_assigned = 0
    decoder_mlp_assigned = 0
    folding_conv_mlp_assigned = 0
    
    for key,value in model_weight_and_bias_dict.items():
        if key.endswith("weights"):
            weights_to_assign += np.array(value).size
            if key.startswith("encoder"):
                encoder_conv_mlp_to_assign += np.array(value).size
            if key.startswith("decoder"):
                decoder_mlp_to_assign += np.array(value).size
            if key.startswith("folding"):
                folding_conv_mlp_to_assign += np.array(value).size

    #set weights and biases manually once.
    #encoder
    encoder_conv0 = pcn.get_e_mlp1()
    weights = (model_weight_and_bias_dict["encoder_0_conv_0_"+weight_prefix])
    biases = (model_weight_and_bias_dict["encoder_0_conv_0_"+bias_prefix])
    encoder_conv0.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["encoder_0_conv_1_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["encoder_0_conv_1_"+bias_prefix])
    encoder_conv0.set_layer_weights(1, weights,biases)
    pcn.set_e_mlp1(encoder_conv0)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    encoder_conv1 = pcn.get_e_mlp2()
    weights = (model_weight_and_bias_dict["encoder_1_conv_0_"+weight_prefix])
    biases = (model_weight_and_bias_dict["encoder_1_conv_0_"+bias_prefix])
    encoder_conv1.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["encoder_1_conv_1_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["encoder_1_conv_1_"+bias_prefix])
    encoder_conv1.set_layer_weights(1, weights,biases)
    #set encoder mlp 1
    pcn.set_e_mlp2(encoder_conv1)
    weights_assigned += np.array(weights).size
    encoder_conv_mlp_assigned += np.array(weights).size

    #decoder
    decoder = pcn.get_d_mlp1()
    weights = (model_weight_and_bias_dict["decoder_fc_0_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["decoder_fc_0_"+bias_prefix])
    decoder.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["decoder_fc_1_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["decoder_fc_1_"+bias_prefix])
    decoder.set_layer_weights(1, weights,biases)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["decoder_fc_2_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["decoder_fc_2_"+bias_prefix])
    decoder.set_layer_weights(2, weights,biases)
    #set decoder mlp 1
    pcn.set_d_mlp1(decoder)
    weights_assigned += np.array(weights).size
    decoder_mlp_assigned += np.array(weights).size

    folding = pcn.get_d_mlp2()
    weights = (model_weight_and_bias_dict["folding_conv_0_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["folding_conv_0_"+bias_prefix])
    folding.set_layer_weights(0, weights,biases)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["folding_conv_1_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["folding_conv_1_"+bias_prefix])
    folding.set_layer_weights(1, weights,biases)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    weights = (model_weight_and_bias_dict["folding_conv_2_"+weight_prefix])
    biases =  (model_weight_and_bias_dict["folding_conv_2_"+bias_prefix])
    folding.set_layer_weights(2, weights,biases)
    #set decoder mlp 2
    pcn.set_d_mlp2(folding)
    weights_assigned += np.array(weights).size
    folding_conv_mlp_assigned += np.array(weights).size
    
    print("assigned {} of {} values".format(weights_assigned, weights_to_assign))
    print("encoder assigned {} of {} values".format(encoder_conv_mlp_assigned, encoder_conv_mlp_to_assign))
    print("decoder assigned {} of {} values".format(decoder_mlp_assigned, decoder_mlp_to_assign))
    print("folding assigned {} of {} values".format(folding_conv_mlp_assigned, folding_conv_mlp_to_assign))

def save_weights_and_biases(pcn, path):
    weight_prefix="weights"
    bias_prefix = "biases"
    model_weight_and_bias_dict = {}
      
    weights_saved = 0
    encoder_conv_mlp_saved = 0
    decoder_mlp_saved = 0
    folding_conv_mlp_saved = 0
    

    #set weights and biases manually once.
    #encoder
    print("Encoder 0 layer 0")
    encoder_conv0 = pcn.get_e_mlp1()
    weights, biases = encoder_conv0.get_layer_weights(0)
    (model_weight_and_bias_dict["encoder_0_conv_0_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["encoder_0_conv_0_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    encoder_conv_mlp_saved += np.array(weights).size
    
    print("Encoder 0 layer 1")
    weights, biases = encoder_conv0.get_layer_weights(1)
    (model_weight_and_bias_dict["encoder_0_conv_1_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["encoder_0_conv_1_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    encoder_conv_mlp_saved += np.array(weights).size
    
    print("Encoder 1 layer 0")
    encoder_conv1 = pcn.get_e_mlp2()
    weights, biases = encoder_conv1.get_layer_weights(0)
    (model_weight_and_bias_dict["encoder_1_conv_0_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["encoder_1_conv_0_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    encoder_conv_mlp_saved += np.array(weights).size
    
    print("Encoder 1 layer 1")
    
    weights, biases = encoder_conv1.get_layer_weights(1)
    (model_weight_and_bias_dict["encoder_1_conv_1_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["encoder_1_conv_1_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    encoder_conv_mlp_saved += np.array(weights).size

    #decoder
    decoder = pcn.get_d_mlp1()
    print("decoder mlp layer 0")
    weights, biases = decoder.get_layer_weights(0)
    (model_weight_and_bias_dict["decoder_fc_0_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["decoder_fc_0_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    decoder_mlp_saved += np.array(weights).size


    
    print("decoder mlp layer 1")
    weights, biases = decoder.get_layer_weights(1)
    (model_weight_and_bias_dict["decoder_fc_1_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["decoder_fc_1_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    decoder_mlp_saved += np.array(weights).size
    
    print("decoder mlp layer 2")
    weights, biases = decoder.get_layer_weights(2)
    (model_weight_and_bias_dict["decoder_fc_2_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["decoder_fc_2_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    decoder_mlp_saved += np.array(weights).size

    print("decoder mlpconv0 layer 0")
    folding = pcn.get_d_mlp2()
    weights, biases = folding.get_layer_weights(0)
    (model_weight_and_bias_dict["folding_conv_0_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["folding_conv_0_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    folding_conv_mlp_saved += np.array(weights).size
    
    print("decoder mlpconv0 layer 1")
    weights, biases = folding.get_layer_weights(1)
    (model_weight_and_bias_dict["folding_conv_1_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["folding_conv_1_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    folding_conv_mlp_saved += np.array(weights).size
    
    print("decoder mlpconv0 layer 2")
    weights, biases = folding.get_layer_weights(2)
    (model_weight_and_bias_dict["folding_conv_2_"+weight_prefix]) = weights
    (model_weight_and_bias_dict["folding_conv_2_"+bias_prefix]) = biases
    weights_saved += np.array(weights).size
    folding_conv_mlp_saved += np.array(weights).size
    
    print("saved {} values".format(weights_saved))
    print("encoder saved {} values".format(encoder_conv_mlp_saved))
    print("decoder saved {} values".format(decoder_mlp_saved))
    print("folding saved {} values".format(folding_conv_mlp_saved))
    output = open(path, 'wb')
    pickle.dump(model_weight_and_bias_dict, output)
    output.close()
    # ModelStructure:
    #     encoder_0/conv_0/biases (DT_FLOAT) [128]
    #     encoder_0/conv_0/weights (DT_FLOAT) [1,3,128]
    #     encoder_0/conv_1/biases (DT_FLOAT) [256]
    #     encoder_0/conv_1/weights (DT_FLOAT) [1,128,256]
        
    #     encoder_1/conv_0/biases (DT_FLOAT) [512]
    #     encoder_1/conv_0/weights (DT_FLOAT) [1,512,512]
    #     encoder_1/conv_1/biases (DT_FLOAT) [1024]
    #     encoder_1/conv_1/weights (DT_FLOAT) [1,512,1024]
        
    #     decoder/fc_0/biases (DT_FLOAT) [1024]
    #     decoder/fc_0/weights (DT_FLOAT) [1024,1024]
    #     decoder/fc_1/biases (DT_FLOAT) [1024]
    #     decoder/fc_1/weights (DT_FLOAT) [1024,1024]
    #     decoder/fc_2/biases (DT_FLOAT) [3072]
    #     decoder/fc_2/weights (DT_FLOAT) [1024,3072]
        
    #     folding/conv_0/biases (DT_FLOAT) [512]
    #     folding/conv_0/weights (DT_FLOAT) [1,1029,512]
    #     folding/conv_1/biases (DT_FLOAT) [512]
    #     folding/conv_1/weights (DT_FLOAT) [1,512,512]
    #     folding/conv_2/biases (DT_FLOAT) [3]
    #     folding/conv_2/weights (DT_FLOAT) [1,512,3]
    
    
    
    
#     full shape
# beta1_power (DT_FLOAT) []
# beta2_power (DT_FLOAT) []
# decoder/fc_0/biases (DT_FLOAT) [1024]
# decoder/fc_0/biases/Adam (DT_FLOAT) [1024]
# decoder/fc_0/biases/Adam_1 (DT_FLOAT) [1024]
# decoder/fc_0/weights (DT_FLOAT) [1024,1024]
# decoder/fc_0/weights/Adam (DT_FLOAT) [1024,1024]
# decoder/fc_0/weights/Adam_1 (DT_FLOAT) [1024,1024]
# decoder/fc_1/biases (DT_FLOAT) [1024]
# decoder/fc_1/biases/Adam (DT_FLOAT) [1024]
# decoder/fc_1/biases/Adam_1 (DT_FLOAT) [1024]
# decoder/fc_1/weights (DT_FLOAT) [1024,1024]
# decoder/fc_1/weights/Adam (DT_FLOAT) [1024,1024]
# decoder/fc_1/weights/Adam_1 (DT_FLOAT) [1024,1024]
# decoder/fc_2/biases (DT_FLOAT) [3072]
# decoder/fc_2/biases/Adam (DT_FLOAT) [3072]
# decoder/fc_2/biases/Adam_1 (DT_FLOAT) [3072]
# decoder/fc_2/weights (DT_FLOAT) [1024,3072]
# decoder/fc_2/weights/Adam (DT_FLOAT) [1024,3072]
# decoder/fc_2/weights/Adam_1 (DT_FLOAT) [1024,3072]

# encoder_0/conv_0/biases (DT_FLOAT) [128]
# encoder_0/conv_0/biases/Adam (DT_FLOAT) [128]
# encoder_0/conv_0/biases/Adam_1 (DT_FLOAT) [128]
# encoder_0/conv_0/weights (DT_FLOAT) [1,3,128]
# encoder_0/conv_0/weights/Adam (DT_FLOAT) [1,3,128]
# encoder_0/conv_0/weights/Adam_1 (DT_FLOAT) [1,3,128]
# encoder_0/conv_1/biases (DT_FLOAT) [256]
# encoder_0/conv_1/biases/Adam (DT_FLOAT) [256]
# encoder_0/conv_1/biases/Adam_1 (DT_FLOAT) [256]
# encoder_0/conv_1/weights (DT_FLOAT) [1,128,256]
# encoder_0/conv_1/weights/Adam (DT_FLOAT) [1,128,256]
# encoder_0/conv_1/weights/Adam_1 (DT_FLOAT) [1,128,256]
# encoder_1/conv_0/biases (DT_FLOAT) [512]
# encoder_1/conv_0/biases/Adam (DT_FLOAT) [512]
# encoder_1/conv_0/biases/Adam_1 (DT_FLOAT) [512]
# encoder_1/conv_0/weights (DT_FLOAT) [1,512,512]
# encoder_1/conv_0/weights/Adam (DT_FLOAT) [1,512,512]
# encoder_1/conv_0/weights/Adam_1 (DT_FLOAT) [1,512,512]
# encoder_1/conv_1/biases (DT_FLOAT) [1024]
# encoder_1/conv_1/biases/Adam (DT_FLOAT) [1024]
# encoder_1/conv_1/biases/Adam_1 (DT_FLOAT) [1024]
# encoder_1/conv_1/weights (DT_FLOAT) [1,512,1024]
# encoder_1/conv_1/weights/Adam (DT_FLOAT) [1,512,1024]
# encoder_1/conv_1/weights/Adam_1 (DT_FLOAT) [1,512,1024]

# folding/conv_0/biases (DT_FLOAT) [512]
# folding/conv_0/biases/Adam (DT_FLOAT) [512]
# folding/conv_0/biases/Adam_1 (DT_FLOAT) [512]
# folding/conv_0/weights (DT_FLOAT) [1,1029,512]
# folding/conv_0/weights/Adam (DT_FLOAT) [1,1029,512]
# folding/conv_0/weights/Adam_1 (DT_FLOAT) [1,1029,512]
# folding/conv_1/biases (DT_FLOAT) [512]
# folding/conv_1/biases/Adam (DT_FLOAT) [512]
# folding/conv_1/biases/Adam_1 (DT_FLOAT) [512]
# folding/conv_1/weights (DT_FLOAT) [1,512,512]
# folding/conv_1/weights/Adam (DT_FLOAT) [1,512,512]
# folding/conv_1/weights/Adam_1 (DT_FLOAT) [1,512,512]
# folding/conv_2/biases (DT_FLOAT) [3]
# folding/conv_2/biases/Adam (DT_FLOAT) [3]
# folding/conv_2/biases/Adam_1 (DT_FLOAT) [3]
# folding/conv_2/weights (DT_FLOAT) [1,512,3]
# folding/conv_2/weights/Adam (DT_FLOAT) [1,512,3]
# folding/conv_2/weights/Adam_1 (DT_FLOAT) [1,512,3]

# global_step (DT_INT32) []