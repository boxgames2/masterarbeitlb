# MasterarbeitLB

## Name
Userfriendly PCN Application 

## Description
This programm is a implementation of the Point Cloud Completion Network. It is adjusted to be used with TF2 and Python 3.x. The programm provides several tools to use PCN to complede custom pointclouds. It also provides the possibility to save the pointcloud completion with the initial scale.

## Installation
System: Windows / Linux 
Python Version: 3.6+

Installation steps:
 - pull the repository
 - if possible, install cuda11+, otherwise the cpu will be used
 - execute `pip install -r requirements.txt`
 - use `python main.py -m "./ArtificialData/sampled_halfChair.pcd" -b "./ArtificialData/ChairBbox_sampled"` to test the installation

## Usage
The program can be used with or without a manually created boundingbox (-b). To start the prgogram, use:

`python main.py -m [path to the model .pcd] -b [path to the folder of the .pcd bbox]`

## Support
For Support contact lbommes@web.de

## Roadmap
Inspiration for further versions: 
 - Convert to keras sequential
 - Add RGB values to the prediction
 - Prepare as Pip package
 - Further training of the PCN Model
## Authors and acknowledgment
Lennard Sven Bommes

## License
Free for use

## Project status
Stable version (WIP)

